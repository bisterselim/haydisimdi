import './Haydi.css';
import Ana from './comp/Ana';
import Gallery from './comp/Gallery';
import Baskan from './comp/Baskan';

import { FaFacebookF, FaInstagram, FaYoutube, FaTwitter } from 'react-icons/fa';


function Haydi() {


  function leftTop(){
    document.getElementById("BGM").style.height = "100%"
    setTimeout(() => {
    document.getElementById("BGM").style.top = "0"
    document.getElementById("BGM").style.position = "fixed"
    document.getElementById("Header").style.width = "100%"
    document.getElementById("slider").style.opacity = "1"
    document.getElementById("Ana").style.display = "flex"
    document.getElementById("Gallery").style.display = "flex"
    document.getElementById("BaskanContent").style.display = "flex"
    document.getElementById("Main").style.position = "relative"

    }, 500);
    document.getElementById("Header").style.boxShadow = "5px 0px 10px 2px black"
    document.getElementById("Text").style.display = "none"
    document.getElementById("Text2").style.display = "flex"

  }

  // function baskanC(){
  //   document.getElementById("BaskanContent").style.display = "flex"
  //   document.getElementById("slider").style.display = "none"
  // }
  // function Gallery(){
  //   document.getElementById("slider").style.display = "flex"
  //   document.getElementById("BaskanContent").style.display = "none"
  // }


  return (
    <div id="Main">

<div id='BGM'onClick={leftTop}>

<div id='BG'>

  <div id='Logo'>

    <div id='TextM'>

      <div id='Text'>
        <svg width="275" height="275" xmlns="http://www.w3.org/2000/svg">
          <path id="circlePath" d="M137.5,137.5 m0,-125 a 125,125 0 1,0 0,250 a 125,125 0 1,0 0,-250" fill="transparent" />
          <text>
            <textPath href="#circlePath" fill="black" fontWeight="bold">
              SENİNLE IŞIĞIMIZ DAHA PARLAK
            </textPath>
          </text>
        </svg>
      </div>

      <div id='Text2'>
        <svg width="275" height="275" xmlns="http://www.w3.org/2000/svg">
          <path id="circlePath" d="M137.5,137.5 m0,-125 a 125,125 0 1,0 0,250 a 125,125 0 1,0 0,-250" fill="transparent" />
          <text>
            <textPath href="#circlePath" fill="#F89A20" fontWeight="bold">
              HAYDİ ŞİMDİ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; SIRA SENDE
            </textPath>
          </text>
        </svg>
      </div>

    </div>

  </div>



</div>
</div>

      <header id='Header'>
        <div id='HeaderAlt'></div>
        <div id='RTEimg'></div>
        <div id='Title'>
          <a href='#ANASAYFA' className="no-underline"><section  id='Home' className='TT'>Ana Sayfa</section ></a>
          <a href='#GALERI' className="no-underline"><section  id='Galerim' className='TT'>Galeri</section ></a>
          <a href='#BASKAN' className="no-underline"><section  id='Baskans' className='TT' >İl Başkanımızdan</section ></a>
        </div>
        <div id='TitleRef'>
          <div id='iletisim'>

          </div>
          <div id='TitleRefAlt'>
            <a href='https://www.facebook.com/profile.php?id=61551859359888&mibextid=ZbWKwL' target='_blank'><FaFacebookF customIcon className="Face"></FaFacebookF></a>
            <a href='https://instagram.com/kutahya.yuzyili?igshid=OGQ5ZDc2ODk2ZA==' target='_blank'><FaInstagram customIcon className="Face"></FaInstagram></a>
            <a href='https://www.youtube.com/channel/UCCvs9LbIdSjxSyPj-c22HLQ' target='_blank'><FaYoutube customIcon className="Face"></FaYoutube></a>
            <a href='https://twitter.com/kutahyayuzyili' target='_blank'><FaTwitter customIcon className="Face"></FaTwitter></a>
          </div>
        </div>
        <div id='Haydiimg'></div>
      </header>

      <Ana/>
      <Gallery/>
      <Baskan/>



    </div>
  );
}

export default Haydi;
