import { render, screen } from '@testing-library/react';
import Haydi from './Haydi';

test('renders learn react link', () => {
  render(<Haydi />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
