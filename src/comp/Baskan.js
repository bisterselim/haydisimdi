import '../Haydi.css';

export default function Baskan() {


    return (

<div id='BaskanContent'>
    <a name='BASKAN'></a>
    <p id='Btitle'>&emsp; &emsp;İl Başkanımızdan</p><br></br>

    &emsp;&emsp;Kıymetli Hemşehrilerim,<br></br>
    <br></br>
    &emsp;AK Partili olmak, her şeyden önce bir davaya sevdalanmak ve o dava uğrunda her saniyeyi değerli kılmak için, canla başla çalışmak demektir.
    ‍<br></br><br></br>
    &emsp;Davamız, kendi medeniyet değerlerimize yaslanıp, milletimizin sevinç, onur ve mutluluğunu esas alarak Türkiye’yi birlik, beraberlik, kardeşlik ve tam bağımsızlık içinde hak ettiği en yüksek konuma ulaştırmak ve bu uğurda göstereceğimiz çabayla hakkın rızasına, milletimizin duasına talip olmak davasıdır.
    <br></br><br></br>
    &emsp;Bütün kalbimizle inanıyoruz ki; AK Parti gibi bir yuvası, Recep Tayyip Erdoğan gibi bir lideri olanlar için, her yeni gün, yeni bir dirilişin, yeni bir şahlanışın adıdır.
    <br></br><br></br>
    &emsp;Bu güzel yolculukta sizleri de üye olarak aramızda görmek istiyoruz. Çünkü üye olmak, “Bu güzel davada ben de varım” demenin en somut şekli, bu güzel yolda farklı ve nitelikli bir sorumluluk üstlenmeye dair en güzel irade beyanıdır.
    <br></br><br></br>
    &emsp;Yeni ve büyük hedeflere,<br></br>
    &emsp;hep birlikte koşmak dileğiyle…
    <br></br>
    <p id='Btitle'>&emsp;&emsp;Mustafa Önsay</p>
    &emsp;AK Parti Kütahya İl Başkanı
</div>


)
}
